![image](https://img-c.udemycdn.com/course/480x270/4665992_a1c3_2.jpg)


# 🚀 Produto Go Finance

Bem-vindo(a). Este é o Produto Go Finance!

O objetivo deste produto é agilizar a sua vida financeira, com ferramentas para o controle financeiro que facilitam o seu dia-a-dia.

# 🧠 Contexto

Tecnologias utilizadas neste produto:
-  React.JS (https://pt-br.reactjs.org/)
-  Next.JS (https://nextjs.org/)
-  Typescript (https://www.typescriptlang.org/)
-  Pipeline End to End no Frontend (https://gitlab.com/marceloeduardo244/go-finance/-/pipelines)
    - Na pipeline temos 2 jobs
        - run_build_test para testar a fase de build da aplicação
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub
- Golang (https://tip.golang.org/doc/go1.20)
- SQLC (https://github.com/kyleconroy/sqlc)
- Golang-migrate (https://github.com/golang-migrate/migrate)
- GIN (https://github.com/gin-gonic/gin)
- Postgres (https://www.postgresql.org/)
- Pgadmin (https://www.pgadmin.org/)
- Testify para os testes unitários (https://github.com/stretchr/testify)
- Pipeline End to End (https://gitlab.com/marceloeduardo244/go-finance/-/pipelines)
    - Na pipeline temos 4 jobs
        - run_migrations para validar se as migrations estão saudáveis e válidas
        - unit_test para rodar todos os testes unitários da aplicação
        - generate_binary para gerar o arquivo binario da app (.bin)
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub
-  Docker (https://docs.docker.com/get-started/overview/)
-  Kubernetes (https://kubernetes.io/docs/concepts/overview/)
-  NGINX Servidor proxy da aplicação
-  Camada de Observabilidade
    -  ginprometheus
    -  Prometheus
    -  Grafana
    -  AlertManager

## Video utilizando o sistema
- https://gitlab.com/marceloeduardo244/go-finance-integrado/-/blob/main/docs/utilizando-o-sistema.mp4

# Camada de Observabilidade da Aplicação
- A camada de Observabilidade serve para facilitar o contole e a gestão da saude das aplicaçoes.
- O Prometheus coleta as metricas da aplicação e armazena.
- O Grafana utiliza os dados do Prometheus para montar as metricas.
- E com o AlertManager vc pode configurar para que sejam observadas condições especificas da app, como se há conexões disponiveis com o database ou se o consumo de Cpu esta muito alto.
- Alem disso, o AlertManager pode mandar mensagens automaticamente para um canal no Slack, avisando sobre um possivel problema de forma autonoma.
- Para acessar o Grafana e visualizar o dashboard de status da App utilize o login abaixo
- user: admin
- password: 123456

# Video utilizando o dashboard de status da aplicação
- link Utilizando o sistema (https://gitlab.com/marceloeduardo244/go-finance-integrado/-/blob/main/docs/utilizando_prometheus_com_grafana_go_finance.mp4)

## Esquema de dados SQL do Produto
- https://gitlab.com/marceloeduardo244/go-finance-integrado/-/blob/main/docs/estrutura_de_dados_do_produto.jpg

## Arquitetura Cluster Kubernetes
- https://gitlab.com/marceloeduardo244/go-finance-integrado/-/blob/main/docs/arquitetura_do_cluster_kubernetes.jpg
- https://drive.google.com/file/d/1yvjzqWXunFSFh-7m51-5G3FNx4iTf7WX/view?usp=sharing

## Arquitetura Docker Compose
- https://gitlab.com/marceloeduardo244/spring-finance-integracao/-/blob/main/docs/arquitetura_da_stack_docker_compose.jpg
- https://drive.google.com/file/d/1UEJ4xx2jsjrusbM-9tebcDYxmAKo5t_G/view?usp=sharing

# Backend do Produto
- https://gitlab.com/marceloeduardo244/go-finance

# Frontend do Produto
- https://gitlab.com/marceloeduardo244/go-finance-web


## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá novos containers rodando na sua maquina.
- Vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
    - 9092 - postgres
    - 9093 - prometheus
    - 9094 - grafana
    - 9095 - alertmanager

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- kubectl apply -f ./kubernetes
- Vc terá novos containers rodando na sua maquina.
- Vc terá tbm o servidor proxy da aplicação, com os seguintes endereços
    - 80 - frontend
    - 9090 - backend
    - 9091 - pgadmin
    - 9092 - prometheus
    - 9093 - grafana
    - 9094 - alertmanager
- Para rodar o servidor execute os comandos abaixo na raiz do projeto
acesse o diretório ./devops/kubernetes/proxy-server/config
Edite o arquivo nginx.conf modificando o valor IPdaMAQUINA pelo ip da maquina onde cluster kubernetes foi iniciado.
- Para subir o container do proxy:
    - Acesse o diretório ./devops/kubernetes/proxy-server/
    - Execute o comando docker-compose up -d

Made with 💜 at OliveiraDev
